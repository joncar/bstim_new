<ul class="clearlist widget-posts">
    <?php $this->db->limit(2)->order_by('fecha','DESC'); ?>
    <?php foreach($this->db->get_where('blog',array('status'=>1,'idioma'=>$_SESSION['lang']))->result() as $b): ?>
        <li class="clearfix">
            <a href="<?= base_url('blog/'.toUrl($b->id.'-'.$b->titulo)) ?>"><img src="<?= base_url('img/blog/'.$b->foto) ?>" alt="" class="widget-posts-img"></a>
            <div class="widget-posts-descr">
                <a href="<?= base_url('blog/'.toUrl($b->id.'-'.$b->titulo)) ?>" title="<?= $b->titulo ?>"><?= cortar_palabras($b->titulo,10).'...' ?></a>
                Publicat per <?= $b->user ?> <?= strftime('%m %b',strtotime($b->fecha)) ?>
            </div>
        </li>
    <?php endforeach ?>
</ul>