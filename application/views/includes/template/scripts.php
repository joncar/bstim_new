<!-- JS -->
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/bootstrap.min.js"></script>        
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/SmoothScroll.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.localScroll.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.viewport.mini.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.countTo.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.appear.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.sticky.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.magnific-popup.min.js"></script>
<!-- Replace test API Key "AIzaSyBfiIZSk_QC7UWgdGekKZLEUXUBi_mWPnE" with your own one below 
**** You can get API Key here - https://developers.google.com/maps/documentation/javascript/get-api-key -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfiIZSk_QC7UWgdGekKZLEUXUBi_mWPnE"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/gmap3.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/wow.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.simple-text-rotator.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/all.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/contact-form.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.ajaxchimp.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/tiltfx.js"></script>     
<script type="text/javascript" src="<?= base_url() ?>theme/theme/js/jquery.mb.YTPlayer.js"></script>       
<!--[if lt IE 10]><script type="text/javascript" src="js/placeholder.js"></script><![endif]-->
<?php if(!empty($_SESSION['cached'])): ?>
	<script>
		$(".contenido").show();
	</script>
<?php endif ?>