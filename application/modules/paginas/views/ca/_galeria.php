<div class="row multi-columns-row mb-30 mb-xs-10">
    

    <?php foreach($this->db->get('galeria')->result() as $g): ?>
	    <div class="col-md-4 col-lg-4 mb-md-10">
	        
	        <div class="post-prev-img">
	            <a href="<?= base_url('img/galeria/'.$g->foto) ?>" class="lightbox-gallery-2 mfp-image" style="width:100%; height:400px; background:url(<?= base_url('img/galeria/'.$g->foto) ?>); background-size:cover; display:block;">
	            	<img src="<?= base_url('img/galeria/'.$g->foto) ?>" style="display: none">
	            </a>
	        </div>

	    </div>
    <?php endforeach ?>
    
    
    
</div>