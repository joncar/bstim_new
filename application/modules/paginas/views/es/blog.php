
        
    <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter --><!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            
            <!-- End About Section -->

            <!-- Section -->
            
            <!-- End Section -->

            
            <!-- End Features Section -->

            <!-- Section -->
            
            <!-- End Section -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            
            

            

            <!-- Section -->
            

            

            <!-- Google Map -->
            



            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            

            

            


            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            


            


            

            


            
                        
            
            
            
             <!-- Section -->
            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
             <!-- Section -->
            
            
            
                      
            <!-- End Head Section -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            <!-- Foter -->
            
            <!-- End Foter -->
        	<!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- Google Map -->
            
            <!-- End Google Map -->
            
            
            <!-- Contact Section -->
            
            <!-- End Contact Section -->
            
            
            
            <!-- Section -->
            
            
            
            
            
           
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            <!-- Section -->
            

			
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            

            <!-- Google Map -->
            


            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            

            <!-- Google Map -->
            


            
            <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter --><!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            
            <!-- End About Section -->

            <!-- Section -->
            
            <!-- End Section -->

            
            <!-- End Features Section -->

            <!-- Section -->
            
            <!-- End Section -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            
            

            

            <!-- Section -->
            

            

            <!-- Google Map -->
            



            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            

            

            


            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            


            


            

            


            
            <div>[menu]</div>            
            <section class="page-section fixed-height-small pt-0 pb-0 bg-dark-alfa-30" data-background="<?= base_url() ?>theme/theme/images/full-width-images/section-bg-12.jpg" style="background-image: url(&quot;images/full-width-images/section-bg-12.jpg&quot;);">
                <div class="js-height-parent container relative" style="height: 600px;">
                    
                    <div class="home-content">
                        <div class="home-text">

                            
                            <h2 class="hs-line-14 font-alt mb-50 mb-xs-30">
                                Noticies
                            </h2>
                            
                        </div>
                    </div>
                    
                </div>
            </section>
            <section class="page-section">
                <div class="container relative">
                    
                    <div class="row">
                        
                        <!-- Content -->
                        <div class="col-sm-8">
                            
                            <div>[foreach:blog]</div>
                            <div class="blog-item">
                                <h2 class="blog-item-title font-alt">
                                    <a href="[link]">[titulo]</a>
                                </h2>
                                <div class="blog-item-data">
                                    <a href="#"><i class="fa fa-clock-o"></i> [fecha]</a>
                                    <span class="separator">&nbsp;</span>
                                    <a href="#"><i class="fa fa-user"></i> [user]</a>
                                    <span class="separator">&nbsp;</span>
                                    <i class="fa fa-folder-open"></i>                                    
                                </div>
                                <div class="blog-media">
                                    <ul class="clearlist content-slider owl-carousel owl-theme" style="opacity: 1; display: block;">
                                        <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1500px; left: 0px; display: block;"><div class="owl-item" style="width: 750px;"><li>
                                            <img src="[foto]" alt="" style="outline: medium none currentcolor; cursor: inherit;">
                                        </li></div></div></div>                                        
                                    <div class="owl-controls clickable" style="display: none;"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div></div><div class="owl-buttons"><div class="owl-prev"><i class="fa fa-angle-left"></i></div><div class="owl-next"><i class="fa fa-angle-right"></i></div></div></div></ul>
                                </div>
                                <div class="blog-item-body">
                                    <p>
                                        [texto]
                                    </p>
                                </div>
                                <div class="blog-item-foot">
                                    <a href="[link]" class="btn btn-mod btn-round  btn-small" style="outline: medium none currentcolor; cursor: inherit;">
                                        Veure més <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                            
                            <div>[/foreach]</div>
                            
                            <!-- Pagination -->
                            <div class="pagination">
                                [paginacion]
                            </div>
                            <!-- End Pagination -->
                            
                        </div>
                        <!-- End Content -->
                        
                        <!-- Sidebar -->
                        <div class="col-sm-4 col-md-3 col-md-offset-1 sidebar">
                            
                            <!-- Search Widget -->
                            <div class="widget">
                                <form class="form-inline form" role="form" action="[base_url]blog">
                                    <div class="search-wrap">
                                        <button class="search-button animate" type="submit" title="Buscar">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <input name="direccion" class="form-control search-field" placeholder="Buscar..." type="text">
                                    </div>
                                </form>
                            </div>
                            <!-- End Search Widget -->
                            
                            <!-- Widget -->
                            <div class="widget">
                                
                                <h5 class="widget-title font-alt">Categories</h5>
                                
                                <div class="widget-body">
                                    <ul class="clearlist widget-menu">
                                        [foreach:categorias]
                                        <li>
                                            <a href="[base_url]blog?categorias_id=[id]" title="">[blog_categorias_nombre]</a>
                                            <small>
                                                - [cantidad]
                                            </small>
                                        </li>
                                        [/foreach]
                                    </ul>
                                </div>
                                
                            </div>
                            <!-- End Widget -->
                            
                            <!-- Widget -->
                            <div class="widget">
                                
                                <h5 class="widget-title font-alt">Tags</h5>
                                
                                <div class="widget-body">
                                    <div class="tags">
                                        [tags]
                                    </div>
                                </div>
                                
                            </div>
                            <!-- End Widget -->
                            
                            <!-- Widget -->
                            <div class="widget">
                                
                                <h5 class="widget-title font-alt">Últimes entradas</h5>
                                
                                <div class="widget-body">
                                    <ul class="clearlist widget-posts">
                                        <div>[foreach:recientes]</div>
                                        <li class="clearfix">
                                            <a href="[link]">
                                                <img src="[foto]" alt="" class="widget-posts-img" style="outline: medium none currentcolor; cursor: inherit;">
                                            </a>
                                            <div class="widget-posts-descr">
                                                <a href="[link]" title="">[titulo]</a>
                                                Escriu per [user] el [fecha] 
                                            </div>
                                        </li>
                                        <div>[/foreach]</div>
                                    </ul>
                                </div>
                                
                            </div>
                            <!-- End Widget -->
                            
                            
                            
                        </div>
                        <!-- End Sidebar -->
                    </div>
                    
                </div>
            </section>
            <div>[footer]</div>