

    <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter -->
            <!-- Home Section -->
            
            <!-- End Home Section -->
            
            
            <!-- About Section -->
            
            
            <!-- Divider -->
            
            <!-- End Divider -->
            
            <!-- Services Section -->
            

            <!-- End Services Section -->
            <!-- Home Section -->
            
            <!-- Google Map -->
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            
            <!-- End About Section -->

            <!-- Section -->
            
            <!-- End Section -->

            
            <!-- End Features Section -->

            <!-- Section -->
            
            <!-- End Section -->
            

            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            
            

            

            <!-- Section -->
            

            

            <!-- Google Map -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            

            

            


            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            


            


            

            


            
                        
            
            
            
             <!-- Section -->
            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
             <!-- Section -->
            
            
            
                      
            <!-- End Head Section -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            <!-- Foter -->
            
            <!-- End Foter -->
        	<!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- Google Map -->
            
            <!-- End Google Map -->
            
            
            <!-- Contact Section -->
            
            <!-- End Contact Section -->
            
            
            
            <!-- Section -->
            
            
            
            
            
           
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            <!-- Section -->
            

			
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            

            <!-- Google Map -->
            


            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            
             <!-- Google Map -->
            


            
            
            <!-- Section -->
            

			
            
            <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter -->
            <!-- Home Section -->
            
            <!-- End Home Section -->
            
            
            <!-- About Section -->
            
            
            <!-- Divider -->
            
            <!-- End Divider -->
            
            <!-- Services Section -->
            

            <!-- End Services Section -->
            <!-- Home Section -->
            
            <!-- Google Map -->
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            
            <!-- End About Section -->

            <!-- Section -->
            
            <!-- End Section -->

            
            <!-- End Features Section -->

            <!-- Section -->
            
            <!-- End Section -->
            <div class="top-bar dark">
    <div class="full-wrapper clearfix">
        <ul class="top-links left">
            <li data-lang="ca"><a href="[base_url]main/traduccion/ca">CAT</a></li>
            <li data-lang="es"><a href="[base_url]main/traduccion/es">ESP</a></li>
            <li data-lang="en"><a href="[base_url]main/traduccion/en">ENG</a></li>
        </ul>
        <ul class="top-links right tooltip-bot" data-original-title="" title="">
            <li><a href="tel:+34938040102" data-original-title="" title=""><i class="fa fa-phone"></i> +34 93 804 01 02</a></li>
            <li><a href="https://www.facebook.com/bstimfair/" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/bstim_fair" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCj9RFqtirasI6aXEn9OZksg" title="" data-original-title="Youtube"><i class="fa fa-youtube"></i></a></li>
            <li><a href="mailto:info@bstim.cat" title="" data-original-title="E-mail"><i class="fa fa-envelope"></i></a></li>
        </ul>
    </div>
</div>
<div id="sticky-wrapper" class="sticky-wrapper" style="height: 75px;"><nav class="main-nav js-stick">
    <div class="full-wrapper relative clearfix">
        <div class="milogo">
            <div class="container">
                <div class="logo milogo">
                    <a href="[base_url]" style="">
                        <img src="[base_url]img/logo.svg" alt="" style="">
                    </a>
                </div>
            </div>
        </div>
        <div class="mobile-nav" style="height: 75px; line-height: 75px; width: 75px;">
            <i class="fa fa-bars"></i>
        </div>
        <div class="inner-nav desktop-nav">
            <ul class="clearlist">
                <li>
                    <a class="mn-has-sub" href="#" style="height: 75px; line-height: 75px; ">
                        Que és? <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="mn-sub">
                        <li>
                            <a href="[base_url]p/que-es" style="">Que és?</a>
                        </li>
                        <li><a href="[base_url]p/mobilitat" style="">Mobilitat</a></li>
                        <li><a href="[base_url]p/ubicacio" style="">Ubicació</a></li>
                        <li><a href="[base_url]p/colaboradores" style="height: 75px; line-height: 75px; ">Col.laboradors</a></li>
                    </ul>
                </li>
                
                <li>
                    <a href="[base_url]p/expositors" class="mn-has-sub" style="height: 75px; line-height: 75px; ">Expositors <i class="fa fa-angle-down"></i></a>
                    <ul class="mn-sub">
                        <li class="visible-xs">
                            <a href="[base_url]p/expositors" style="">Expositors</a>
                        </li>
                        <li>
                            <a href="[base_url]p/expositors#listado" style="">Llistat d'expositors</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="mn-has-sub" style="height: 75px; line-height: 75px; ">Acreditacions <i class="fa fa-angle-down"></i></a>
                    <ul class="mn-sub">
                        <li>
                            <a href="[base_url]p/acreditaciones-estudiante" style="">Estudiant</a>
                        </li>
                        <li>
                            <a href="[base_url]p/acreditaciones-empresa" style="">Empresa</a>
                        </li>
                    </ul>
                </li>
                <li><a href="[base_url]blog" style="height: 75px; line-height: 75px; ">Premsa</a></li>
                <li><a href="[base_url]p/galeria" style="height: 75px; line-height: 75px; ">Galeria</a></li>
                <li><a href="[base_url]p/contacto" style="height: 75px; line-height: 75px; ">Contacte</a></li>
                <li><a href="[base_url]p/activitats" style="height: 75px; line-height: 75px; ">Activitats</a></li>
                <li><a href="[base_url]p/activitats" style="height: 75px; line-height: 75px; ">Edició Anteriors</a></li>
                <li><a href="[base_url]panel" style="height: 75px; line-height: 75px; "><span class="btn btn-mod btn-circle btn-gray"><i class="fa fa-lock"></i></span></a></li>
            </ul>
        </div>
    </div>
</nav></div>