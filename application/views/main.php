<?php 
	
	$this->load->view('read',array('page'=>$this->load->view($this->theme.'main',array('blog'=>$blog),TRUE,'paginas')),FALSE,'paginas'); 

?>

<?php if(empty($_SESSION['cached'])): $_SESSION['cached'] = 1; ?>
	<div class="popup">
		<img src="<?= base_url('img/logopopup.svg') ?>" class="logopopup">		
		<div style="position: absolute; bottom:10%; width:100%; text-align: center"><a href="javascript:cerrarPopup()" class="btn btn-mod btn-large btn-round">Ir a la versión 2017</a></div>
		<div class="popuptexto" style="">
			<div>6 | 7 marzo’19 Igualada (Barcelona) </div>
			<h1 class="lead text-mobile mt-0 mb-40 mb-xs-20">Textile & <br/>Manufacturing</h1>
			<span>Las mejores soluciones para el textil</span>
		</div>
	</div>
	<script>
		function cerrarPopup(){
			$(".popup").remove();
			$(".contenido").show();
		}
	</script>
<?php endif ?>