
        
    <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter --><!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            
            <!-- End About Section -->

            <!-- Section -->
            
            <!-- End Section -->

            
            <!-- End Features Section -->

            <!-- Section -->
            
            <!-- End Section -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            
            

            

            <!-- Section -->
            

            

            <!-- Google Map -->
            



            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            

            

            


            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            


            


            

            


            
                        
            
            
            
             <!-- Section -->
            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
             <!-- Section -->
            
            
            
                      
            <!-- End Head Section -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            <!-- Foter -->
            
            <!-- End Foter -->
        	<!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- Google Map -->
            
            <!-- End Google Map -->
            
            
            <!-- Contact Section -->
            
            <!-- End Contact Section -->
            
            
            
            <!-- Section -->
            
            
            
            
            
           
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            <!-- Section -->
            

			
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            

            <!-- Google Map -->
            


            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            

            <!-- Google Map -->
            


            
            <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            <footer class="small-section bg-gray-lighter footer pb-60">
                <div class="container">
                    
                    <!-- Footer Widgets -->
                    <div class="row align-left">
                        
                        <!-- Widget -->
                        <div class="col-sm-6 col-md-3">
                            <div class="widget">
                                
                                <h5 class="widget-title font-alt">About</h5>
                                
                                <div class="widget-body">
                                    <div class="widget-text clearfix">
                                        
                                        Purus ut dignissim consectetur, nulla erat ultrices purus, ut consequat sem elit non sem.
                                        Quisque magna ante eleifend lorem eleifend. Mauris pretium etere lorem enimet scelerisque.   
                                    
                                    </div>
                                </div>
                                
                            </div>                            
                        </div>
                        <!-- End Widget -->
                        
                        <!-- Widget -->
                        <div class="col-sm-6 col-md-3">
                            <div class="widget">
                                
                                <h5 class="widget-title font-alt">Additional Links</h5>
                                
                                <div class="widget-body">
                                    <ul class="clearlist widget-menu">
                                        <li>
                                            <a href="#" title="" style="outline: medium none currentcolor; cursor: inherit;">About Us <i class="fa fa-angle-right right"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" title="" style="outline: medium none currentcolor; cursor: inherit;">Careers <i class="fa fa-angle-right right"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" title="" style="outline: medium none currentcolor; cursor: inherit;">Privacy Policy <i class="fa fa-angle-right right"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" title="" style="outline: medium none currentcolor; cursor: inherit;">Contact <i class="fa fa-angle-right right"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                
                            </div>                          
                        </div>
                        <!-- End Widget -->
                        
                        <!-- Widget -->
                        <div class="col-sm-6 col-md-3">
                            <div class="widget">
                                
                                <h5 class="widget-title font-alt">Latest posts</h5>
                                
                                <div class="widget-body">
                                    <ul class="clearlist widget-posts">
                                        <li class="clearfix">
                                            <a href="" style="outline: medium none currentcolor; cursor: inherit;"><img src="<?= base_url() ?>theme/theme/images/blog/previews/post-prev-3.jpg" alt="" class="widget-posts-img" style="outline: medium none currentcolor; cursor: inherit;"></a>
                                            <div class="widget-posts-descr">
                                                <a href="#" title="" style="outline: medium none currentcolor; cursor: inherit;">New Web Design Trends in 2015 year</a>
                                                Posted by John Doe 7 Mar 
                                            </div>
                                        </li>
                                        <li class="clearfix">
                                            <a href="" style="outline: medium none currentcolor; cursor: inherit;"><img src="<?= base_url() ?>theme/theme/images/blog/previews/post-prev-4.jpg" alt="" class="widget-posts-img" style="outline: medium none currentcolor; cursor: inherit;"></a>
                                            <div class="widget-posts-descr">
                                                <a href="#" title="" style="outline: medium none currentcolor; cursor: inherit;">Hipster’s Style in Web Design and Logo</a>
                                                Posted by John Doe 7 Mar 
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                
                            </div>
                            <!-- End Widget -->                        
                        </div>
                        <!-- End Widget -->
                        
                        <!-- Widget -->
                        <div class="col-sm-6 col-md-3">
                            <div class="widget">
                                
                                <h5 class="widget-title font-alt">Newsletter</h5>
                                
                                <div class="widget-body">
                                    <div class="widget-text clearfix">
                                        
                                        <form class="form" id="mailchimp" novalidate="true">
                                            
                                            <div class="mb-20">Stay informed with our newsletter. Please trust us, we will never send you spam.</div>
                                            
                                            <div class="mb-20">
                                                <input placeholder="Enter Your Email" class="form-control input-md round mb-10" pattern=".{5,100}" required="" name="EMAIL" type="email">
                                                <button type="submit" class="btn btn-mod btn-gray btn-medium btn-round form-control mb-xs-10" style="outline: medium none currentcolor; cursor: inherit;">Subscribe</button>
                                            </div>
                                              
                                            <div id="subscribe-result"></div>
                                        </form>
                                    
                                    </div>
                                </div>
                                
                            </div>                            
                        </div>
                        <!-- End Widget -->
                        
                    </div>
                    <!-- End Footer Widgets -->
                    
                    <!-- Divider -->
                    <hr class="mt-0 mb-80 mb-xs-40">
                    <!-- End Divider -->
                    
                    <!-- Footer Logo -->
                    <div class="local-scroll mb-30 wow fadeInUp animated animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeInUp;">
                        <a href="#top" style="outline: medium none currentcolor; cursor: inherit;"><img src="<?= base_url() ?>theme/theme/images/logo-footer.png" alt="" style="outline: medium none currentcolor; cursor: inherit;" width="78" height="36"></a>
                    </div>
                    <!-- End Footer Logo -->
                    
                    <!-- Social Links -->
                    <div class="footer-social-links mb-110 mb-xs-60">
                        <a href="#" title="Facebook" target="_blank" style="outline: medium none currentcolor; cursor: inherit;"><i class="fa fa-facebook"></i></a>
                        <a href="#" title="Twitter" target="_blank" style="outline: medium none currentcolor; cursor: inherit;"><i class="fa fa-twitter"></i></a>
                        <a href="#" title="Behance" target="_blank" style="outline: medium none currentcolor; cursor: inherit;"><i class="fa fa-behance"></i></a>
                        <a href="#" title="LinkedIn+" target="_blank" style="outline: medium none currentcolor; cursor: inherit;"><i class="fa fa-linkedin"></i></a>
                        <a href="#" title="Pinterest" target="_blank" style="outline: medium none currentcolor; cursor: inherit;"><i class="fa fa-pinterest"></i></a>
                    </div>
                    <!-- End Social Links -->  
                    
                    <!-- Footer Text -->
                    <div class="footer-text">
                        
                        <!-- Copyright -->
                        <div class="footer-copy font-alt">
                            <a href="http://themeforest.net/user/theme-guru/portfolio" target="_blank" style="outline: medium none currentcolor; cursor: inherit;">© Rhythm 2017</a>.
                        </div>
                        <!-- End Copyright -->
                        
                        <div class="footer-made">
                            Made with love for great people.
                        </div>
                        
                    </div>
                    <!-- End Footer Text --> 
                    
                 </div>
                 
                 
                 <!-- Top Link -->
                 <div class="local-scroll">
                     <a href="#top" class="link-to-top" style="outline: medium none currentcolor; cursor: inherit;"><i class="fa fa-caret-up"></i></a>
                 </div>
                 <!-- End Top Link -->
                 
            </footer>
            <!-- End Foter -->