<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        } 
        
        function loadView($view = ''){
            if(is_string($view)){
                $output = $this->load->view($view,array(),TRUE);
                $view = array('view'=>'panel','crud'=>'user','output'=>$output);
            }
            parent::loadView($view);
        }
        
        function acreditaciones_estudiantes(){
			$crud = $this->crud_function('','');
			$crud = $crud->render();
			$this->loadView($crud);
        }

        function acreditaciones_empresas(){
			$crud = $this->crud_function('','');
			$crud = $crud->render();
			$this->loadView($crud);
        }

        function solicitudes_espacios(){
			$crud = $this->crud_function('','');
			$crud = $crud->render();
			$this->loadView($crud);
        }
    }
?>
