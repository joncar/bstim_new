

    <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter -->
            <!-- Home Section -->
            
            <!-- End Home Section -->
            
            
            <!-- About Section -->
            
            
            <!-- Divider -->
            
            <!-- End Divider -->
            
            <!-- Services Section -->
            

            <!-- End Services Section -->
            <!-- Home Section -->
            
            <!-- Google Map -->
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            
            <!-- End About Section -->

            <!-- Section -->
            
            <!-- End Section -->

            
            <!-- End Features Section -->

            <!-- Section -->
            
            <!-- End Section -->
            

            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            
            

            

            <!-- Section -->
            

            

            <!-- Google Map -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            

            

            


            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            


            


            

            


            
                        
            
            
            
             <!-- Section -->
            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
             <!-- Section -->
            
            
            
                      
            <!-- End Head Section -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            <!-- Foter -->
            
            <!-- End Foter -->
        	<!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- Google Map -->
            
            <!-- End Google Map -->
            
            
            <!-- Contact Section -->
            
            <!-- End Contact Section -->
            
            
            
            <!-- Section -->
            
            
            
            
            
           
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            <!-- Section -->
            

			
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            

            <!-- Google Map -->
            


            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            
             <!-- Google Map -->
            


            
            
            <!-- Section -->
            

			
            
            <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            <footer class="small-section bg-gray-lighter footer pb-60">
                <div class="container">
                    
                    <!-- Footer Widgets -->
                    <div class="row align-left">
                        
                        <!-- Widget -->
                        <div class="col-sm-6 col-md-3">
                            <div class="widget">
                                
                                <h5 class="widget-title font-alt editContent" style="">QUÈ ÉS BSTIM? </h5>
                                
                                <div class="widget-body">
                                    <div class="widget-text clearfix editContent" style="">BSTIM és un saló professional que pretén donar resposta a les necessitats d’aprovisionament en proximitat de peça acabada de punt de marques i distribuïdors del sector de la moda.</div>
                                </div>
                                
                            </div>                            
                        </div>
                        <!-- End Widget -->
                        
                        <!-- Widget -->
                        <div class="col-sm-6 col-md-3">
                            <div class="widget">
                                
                                <h5 class="widget-title font-alt">Enlaces</h5>
                                
                                <div class="widget-body">
                                    <ul class="clearlist widget-menu">
                                        <li>
                                            <a title="" style="" href="[base_url]p/que-es">Que és?<i class="fa fa-angle-right right"></i></a>
                                        </li>
                                        <li>
                                            <a title="" style="" href="[base_url]p/contacto">Contacte<i class="fa fa-angle-right right"></i></a>
                                        </li>
                                        <li>
                                            <a title="" style="" href="[base_url]p/aviso-legal">Avis legal<i class="fa fa-angle-right right"></i></a>
                                        </li>
                                        <li>
                                            <a title="" href="[base_url]p/colaboradores" style="">Col.laboradors<i class="fa fa-angle-right right"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                
                            </div>                          
                        </div>
                        <!-- End Widget -->
                        
                        <!-- Widget -->
                        <div class="col-sm-6 col-md-3">
                            <div class="widget">
                                
                                <h5 class="widget-title font-alt">Últimes notícies</h5>
                                
                                <div class="widget-body">
                                    [blog_footer]
                                </div>
                                
                            </div>
                            <!-- End Widget -->                        
                        </div>
                        <!-- End Widget -->
                        
                        <!-- Widget -->
                        <div class="col-sm-6 col-md-3">
                            <div class="widget">
                                
                                <h5 class="widget-title font-alt">Inscriu-te al butlletí </h5>
                                
                                <div class="widget-body">
                                    <div class="widget-text clearfix">
                                        
                                        <form class="form" id="mailchimp" novalidate="true">
                                            
                                            <div class="mb-20">Et mantindrem informat de les nostres ofertes i novetats. No serem pesats, només volem que estiguis a la última</div>
                                            
                                            <div class="mb-20">
                                                <input class="form-control input-md round mb-10" pattern=".{5,100}" required="" name="EMAIL" placeholder="escriu el teu email" type="email">
                                                <button type="submit" class="btn btn-mod btn-gray btn-medium btn-round form-control mb-xs-10" style="">Enviar</button>
                                            </div>
                                              
                                            <div id="subscribe-result"></div>
                                        </form>
                                    
                                    </div>
                                </div>
                                
                            </div>                            
                        </div>
                        <!-- End Widget -->
                        
                    </div>
                    <!-- End Footer Widgets -->
                    
                    <!-- Divider -->
                    <hr class="mt-0 mb-80 mb-xs-40">
                    <!-- End Divider -->
                    
                    <!-- Footer Logo -->
                    <div class="local-scroll mb-30 wow fadeInUp animated" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeInUp;">
                        <a href="#top" style=""><img alt="" src="[base_url]img/logo.svg" style="" width="78" height="36"></a>
                    </div>
                    <!-- End Footer Logo -->
                    
                    <!-- Social Links -->
                    <div class="footer-social-links mb-110 mb-xs-60">
                        <a title="Facebook" target="_blank" href="https://www.facebook.com/bstimfair/" style=""><i class="fa fa-facebook"></i></a>
                        <a title="Twitter" target="_blank" href="https://twitter.com/bstim_fair" style=""><i class="fa fa-twitter"></i></a>
                        <a href="https://www.youtube.com/channel/UCj9RFqtirasI6aXEn9OZksg" title="youtube" target="_blank" style=""><i class="fa fa-youtube"></i></a>
                        <a target="_blank" href="mailto:info@bstim.cat" title="E-mail" style=""><i class="fa fa-envelope"></i></a>
                        
                    </div>
                    <!-- End Social Links -->  
                    
                    <!-- Footer Text -->
                    <div class="footer-text">
                        
                        <!-- Copyright -->
                        <div class="footer-copy font-alt">
                            <a target="_blank" href="#" style="">© Copyright 2018 by BSTIM</a>.
                        </div>
                        <!-- End Copyright -->
                        
                        <div class="footer-made">
  <a href="http://www.jordimagana.com" style="">Web by Jordi Magaña</a>
</div>
                        
                    </div>
                    <!-- End Footer Text --> 
                    
                 </div>
                 
                 
                 <!-- Top Link -->
                 <div class="local-scroll">
                     <a href="#top" class="link-to-top" style=""><i class="fa fa-caret-up"></i></a>
                 </div>
                 <!-- End Top Link -->
                 
            </footer>
            <!-- End Foter -->