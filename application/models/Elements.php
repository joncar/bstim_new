<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function galeria(){
		$this->db->order_by('orden','ASC');		
		$galeria = $this->db->get('galeria');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/galeria/'.$g->foto);
		}
		return $galeria;
	}
}