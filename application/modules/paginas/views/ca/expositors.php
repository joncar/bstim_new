

    <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter -->
            <!-- Home Section -->
            
            <!-- End Home Section -->
            
            
            <!-- About Section -->
            
            
            <!-- Divider -->
            
            <!-- End Divider -->
            
            <!-- Services Section -->
            

            <!-- End Services Section -->
            <!-- Home Section -->
            
            <!-- Google Map -->
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            
            <!-- End About Section -->

            <!-- Section -->
            
            <!-- End Section -->

            
            <!-- End Features Section -->

            <!-- Section -->
            
            <!-- End Section -->
            

            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            
            

            

            <!-- Section -->
            

            

            <!-- Google Map -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            

            

            


            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            


            


            

            


            
                        
            
            
            
             <!-- Section -->
            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
             <!-- Section -->
            
            
            
                      
            <!-- End Head Section -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            <!-- Foter -->
            
            <!-- End Foter -->
        	<!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- Google Map -->
            
            <!-- End Google Map -->
            
            
            <!-- Contact Section -->
            
            <!-- End Contact Section -->
            
            
            
            <!-- Section -->
            
            
            
            
            
           
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            <!-- Section -->
            

			
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            

            <!-- Google Map -->
            


            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            
             <!-- Google Map -->
            


            
            
            <!-- Section -->
            

			
            
            <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter -->
            <!-- Home Section -->
            
            <!-- End Home Section -->
            
            
            <!-- About Section -->
            
            
            <!-- Divider -->
            
            <!-- End Divider -->
            
            <!-- Services Section -->
            

            <!-- End Services Section -->
            <!-- Home Section -->
            
            <!-- Google Map -->
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            
            <!-- End About Section -->

            <!-- Section -->
            
            <!-- End Section -->

            
            <!-- End Features Section -->

            <!-- Section -->
            
            <!-- End Section -->
            

            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            
            

            

            <!-- Section -->
            

            

            <!-- Google Map -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            

            

            


            
            
            <div>[menu]</div>
            <!-- Section -->
            <section class="bg bg1 page-section fixed-height-small pt-0 pb-0 bg-dark-alfa-30" data-background="<?= base_url() ?>theme/theme/images/uploads/1/expositorca.jpg" style="background-image: url(&quot;images/uploads/1/expositorca.jpg&quot;);" src="<?= base_url() ?>theme/theme/images/uploads/1/expositorca.jpg">
                <div class="js-height-parent container relative" style=";">
                    
                    <div class="home-content">
                        <div class="home-text">

                            
                            <h2 class="editContent hs-line-14 font-alt mb-50 mb-xs-30" style="">
                                Expositors
                            </h2>
                            
                        </div>
                    </div>
                    
                </div>
            </section>
            <!-- End Section -->
            
            
            
            <section class="page-section">
                <div class="container relative">
                    
                    <!-- Row -->
                    <div class="row">
                        
                        <!-- Col -->
                        
                        <div class="col-sm-6 mb-40">
                            
                            <div class="text editContent" style="">
                                <h3 class="uppercase">PERQUÈ BSTIM?</h3>Des de Fagepi i Fira d’Igualada, s’ha detectat que moltes marques capdavanteres busquen rapidesa, disseny, qualitat i innovació en els seves produccions, factors que poden trobar només en la producció local.</div>
                            
                        </div>

                        <div class="col-sm-6 mb-40">
                            
                            <div class="text editContent" style="">
                                <h3 class="uppercase">&nbsp;</h3>És per això que s’ha ideat BSTIM, una fira que mostri les diferents solucions de producció tèxtil locals, territori amb gran trajectòria i Know How en la matèria, i que atregui a diferents perfils professionals del sector tèxtil i moda.</div>
                            
                        </div>
                        
                     </div>
                    <!-- End Row -->

                    <div class="row">
                        
                        <!-- Col -->
                        
                        <div class="col-sm-6 mb-40">
                            
                            <div class="text editContent"><br><h3 class="uppercase">QUI VE A BSTIM?</h3><p><b>RESPONSABLES D’APROVISIONAMENT I COMPRADORS</b> de producte acabat, dissenyadors, responsables de producte i responsables de col•lecció o directors artístics de marques de moda i distribuïdors del sector tèxtil i moda d’arreu d’Europa.</p><p> </p><p><b>DISSENYADORS INDEPENDENTS</b> que desenvolupen la seva pròpia col•lecció.</p><p><b>CADENES DE BOTIGUES</b> interessades en identificar productors per cobrir necessitats de producte en els seus establiments comercials.</p><p><b>ALUMNES D’ESCOLES DE DISSENY</b> i Escoles de negoci, màrqueting i comunicació dedicades al sector de la moda.</p></div>
                            
                        </div>

                        <div class="col-sm-6 mb-40">
                            
                            <div class="text editContent" style="">
                                <h3 class="uppercase">&nbsp;</h3><br><div>L’ entrada pels visitants és gratuïta, però cal inscripció prèvia mitjançant la qual s’acredita pertànyer al sector.<br><br></div><div><p>Per a facilitar el desplaçament de visitants a BSTIM s’oferirà transport gratuït Barcelona-Igualada.<br></p></div></div>
                            
                        </div>
                        
                     </div>
                    <!-- End Row -->
                    
                    <hr class="mb-40">

                    <div class="row">
                        
                        
                        
                        

                        <div class="col-sm-4 mb-40 editContent " style="">
                            
                            <a class="btn btn-mod btn-block btn-border btn-large btn-round" style="" href="[base_url]files/cataleg-expositors-catweb.pdf">DOSSIER INFORMACIÓ EXPOSITORS</a>
                            
                        </div>
<div class="col-sm-4 mb-40 editContent " style="">
                            
                            <a class="btn btn-mod btn-block btn-border btn-large btn-round" style="" href="http://www.bstim.cat/blog/boletines/frontend/ver/12">INSTRUCCIONS PER A EXPOSITORS</a>
                            
                        </div>

                        <div class="col-sm-4 mb-40 editContent " style="">
                            
                            <a class="btn btn-mod btn-block btn-border btn-large btn-round" style="" href="[base_url]files/catalegcat.pdf">CATÀLEG 2017</a>
                            
                        </div>
                        
                     </div>
                    <!-- End Row -->

                </div>
            </section>


            <section class="page-section bg-dark">
                <div class="container relative">
                    
                    <!-- Row -->
                    <div class="row">
                        
                        <!-- Col -->
                        
                        <div class="col-sm-6 mb-40">
                            
                            <div class="editContent" style="content: none; color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, arial, sans-serif;">
                                <h3 class="uppercase" style="color:white">QUI POT SER EXPOSITOR DE BSTIM?</h3><div>BSTIM està orientat a ser un aparador per a, principalment, empreses industrials que fabriquen peces acabades de confecció, gènere de punt i productes de bany amb capacitat per sevir producte acabat i adaptar-se a diferents volums i condicions de fabricació.</div><div><p><br></p><p>També es pretén acollir a empreses dedicades a la gestió de la producció de peces tèxtils que tenen un establiment permanent a l’Estat Espanyol i un equip professional dedicat al desenvolupament i gestió de produccions per marques de moda i distribuïdors.
</p></div></div>
                            
                        </div>

                        <div class="col-sm-6 mb-40 editContent " style="">
                            
                            <img src="<?= base_url() ?>theme/theme/images/uploads/1/escor1-crop-u51755.jpg" style="width: 100%; ">
                            
                        </div>
                        
                     </div>

                     <!-- Row -->
                    <div class="row">
                        
                        <!-- Col -->
                        
                        <div class="col-sm-6 mb-40">
                            
                            <div class="editContent" style="content: none; color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, arial, sans-serif;"><h3 class="uppercase" style="color:white">ON TÉ LLOC BSTIM?</h3><div>El saló té lloc a Igualada, ciutat amb llarga tradició tèxtil en la que desenvolupen la seva activitat 145 empreses i 2.500 professionals del sector tèxtil i moda que generen una facturació agregada de 350 milions d’euros.<p><br>El clúster Tèxtil i Moda de la ciutat d’Igualada és uns dels principals territoris europeus de fabricació d’articles de gènere de punt exterior (tricotoses rectilínies) a nivell Europeu.<br><br>A Igualada es fabriquen anualment 6 milions de peces de punt per a marques i distribuïdors de tota Europa.<br><br>L’espai que acull el saló és L’Escorxador, un conjunt arquitectònic emblemàtic de la ciutat, d’estil modernista.</p><p><a href="[base_url]p/ubicacio" style="color: #257A97;">Vols saber-ne més?</a></p></div></div>
                            
                        </div>

                        <div class="col-sm-6 mb-40">
                            
                            <img src="<?= base_url() ?>theme/theme/images/uploads/1/escort2-crop-u51767.jpg" style="width: 100%;">
                            
                        </div>
                        
                     </div>

                </div>
            </section>


            <section class="page-section" id="listado">
                <div class="container relative">

                    <!-- Toggle -->
                    <div class="row mb-40">
                    <dl class="toggle editContent active" style="">
                        <dt style="text-align:center">
                            <a href="" class="negro">Llistat d'expositors</a>
                        </dt>
                        <dd style="display: none;">
                            <div class="row">
                                <div class="col-sm-4">
                                    <ul class="list0 nls-None" id="u67943-65">
        <li id="u67943-2">ACTE ESPANYA (IGUALADA)</li>
        <li id="u67943-4">ASSOCIACIÓ GLOBAL TEXTIL BCN (NAVARCLES)</li>
        <li id="u67943-6">ATP (PORTUGAL)</li>
        <li id="u67943-8">BORAS STAD / ACTE (SWEDEN)</li>
        <li id="u67943-10">BOTTON (IGUALADA)</li>
        <li id="u67943-12">CIMTEXTRICOT S.L. (ÒDENA)</li>
        <li id="u67943-14">CONFECCIONES DELMAPUNTO (HELLÍN)</li>
        <li id="u67943-16">DIUTEX IGUALADA (IGUALADA)</li>
        <li id="u67943-18">DOCOR (MATARÓ)</li>
        <li id="u67943-20">DR.KID ( INARBEL) (PORTUGAL)</li>
        <li id="u67943-22">ESCOLA MUNICIPAL D'ART GASPAR CAMPS (IGUALADA)</li>
        <li id="u67943-24">ESTAMPATS SABATER MASANA (CAPELLADES)</li>
        <li id="u67943-26">FANTEXFIL (SABADELL)</li>
        <li id="u67943-28">FITEX</li>
        <li id="u67943-30">GRINTINT&nbsp; (GIRONA)</li>
        <li id="u67943-32">GUASCH HERMANOS&nbsp; (CAPELLADES)</li>
        <li id="u67943-34">HILATURAS FERRER S.A (BANYERES DE MARIOLA)</li>
        <li id="u67943-36">INGENITEX (CALDES DE MONTBUI)</li>
        <li id="u67943-38">INSTITUT DE TERRASSA (TERRASSA)</li>
        <li id="u67943-40">INSTITUT GUINDÀVOLS (LLEIDA)</li><li id="u67943-46">IPUNT KNIT 2000 (ÓDENA)</li>

       </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul>

        
        <li id="u67943-48">JOHN-FIL SCCL (L’ESPLUGA CALBA)</li>
        <li id="u67943-50">KIDAM (EL PONT DE VILUMARA I ROCAFORT)</li>
        <li id="u67943-52">KIT DSIGNS (OLOST DE LLUÇANÈS)</li>
        <li id="u67943-54">LANERA GALICIA (CULLEREDO-SANTO ESTEVO)</li>
        <li id="u67943-56">LAYRET-STOLL (GRANOLLERS)</li>
        <li id="u67943-58">MAITE ROMERO VIDAL (IGUALADA)</li>
        <li id="u67943-60">MALHAS CARJOR (PORTUGAL)</li>
        <li id="u67943-62">M. GUINART S.A. (OLOT)</li>
        <li id="u67943-64">MOMAD METROPOLIS (MADRID)</li>
  <li id="u89916-2">OFICINA DE CAPTACIÓ D´INVERSIONS (IGUALADA)</li>
        <li id="u89916-4">PANTONE - WACOM (ÒDENA)</li>
        <li id="u89916-6">PRACTICS (IGUALADA)</li>
        <li id="u89916-8">PROTEKO-NORDISK DESIGN SKOLA (SUECIA)</li>
        <li id="u89916-10">PUIMAR (IGUALADA)</li>
        <li id="u89916-12">REPROGRAFIA INDUSTRIAL DE CATALUNYA (RUBÍ)</li><li id="u89916-14">RUBEN GANDIA E HIJOS S.L (ALCOY)</li>
                <li id="u67943-42">INSTITUT LLUIS DOMENECH MUNTANER (CANET DE MAR)</li>
        <li id="u67943-44">INTAREX (IGUALADA)</li><li id="u89916-16">SERVISIMÓ IGUALADA S.L. (IGUALADA)</li>

                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul class="list0 nls-None" id="u89916-61">
  
        
        <li id="u89916-18">SHIMA SEIKI (BARBERÀ DEL VALLÈS)</li>
        <li id="u89916-20">STITCH WAY BCN A.I.E (IGUALADA)</li>
        <li id="u89916-22">S. VILARRASSA (BESALÚ)</li>
        <li id="u89916-24">TAYLOR TRADING COMPANY (SANT VICENÇ DE MONTALT)</li>
                <li id="u89916-26">TEGOMAR (IGUALADA)</li>
        <li id="u89916-28">TEIXINT PROJECTES (IGUALADA)</li>
        <li id="u89916-30">TEXDUEÑAS (IGUALADA)</li>
        <li id="u89916-32">TEXTIL ELTEX (MATARÓ)</li>
        <li id="u89916-34">TEXTIL PARERA (NAVARCLES)</li>
        <li id="u89916-36">TEXTIL TAPIAS, S.L. (LA AMETLLA DE MEROLA)</li>
        <li id="u89916-38">TEXTILES CAMPILLO (CUENCA)</li>
        <li id="u89916-40">TINTES EGARA (TERRASSA)</li>
        <li id="u89916-42">TORCIDOS &amp; FANTASIA (ALBACETE)</li>
        <li id="u89916-44">TORRAS WOLD (VACARISSES)</li>
        <li id="u89916-46">TRILOGI ECOMMERCE (IGUALADA)</li>
        <li id="u89916-48">TRITALL (SANT MARTÍ DE TOUS)</li>
        <li id="u89916-50">UNIVERSAL DEL PUNT (TERRASSA)</li>
        <li id="u89916-52">VENETONRED S.L. (IGUALADA)</li>
        <li id="u89916-54">VISUAL TEXTIL (CASTELLAR DEL VALLÈS)</li>
        <li id="u89916-56">VITAMINA C (EL MASNOU)</li>
        <li id="u89916-58">WHAT A PRETTY (ESPARREGUERA)</li>
        <li id="u89916-60">XISOCHO LDA (PORTUGAL)</li>
       </ul>
                                </div>
                            </div>
                        </dd>
                    </dl>
                </div>
                    <!-- End Toggle -->
                    <!-- Row -->
                    <div class="row mb-40">
                        
                        <!-- Col -->
                        
                        <div class="col-sm-6">
                            
                            <div class="text editContent" style="">
                                <h3 class="uppercase">QUAN?</h3>El 22 i 23 de febrer de 2017 de 10 a 19h.</div>
                            
                        </div>

                        <div class="col-sm-6 mb-40">
                            
                            <div class="text editContent " style="">
                                <h3 class="uppercase">COM ARRIBAR?</h3>
                                <img src="<?= base_url() ?>theme/theme/images/mapa.jpg" style="">
                            </div>
                            
                        </div>
                        
                     </div>
                    <!-- End Row -->
                    <!-- Row -->
                    
                    <!-- End Row -->
                </div>
            </section>

            <section class="page-section bg-dark">
                <div class="container relative">   
                    <div class="row mb-40">
                            
                            <!-- Col -->
                            
                            <div class="col-sm-12">
                                
                                <div class="text editContent" style="text-align: center; color: white; ">
                                    <h3 class="uppercase" style="color:white">DESITJA CONTRACTAR UN ESPAI?</h3>Contacti amb nosaltres:<br>Si ho prefereix, empleni el formulari a continuació i ens posarem en contacte amb vostè.</div>
                                
                            </div>
                        
                     </div>                 
                    <!-- Row -->
                    <form method="post" onsubmit="return contacto(this)">
                                <div class="clearfix">
                                    
                                    <div class="cf-left-col">
                                        
                                        <!-- Name -->
                                        <div class="form-group">
                                            <input id="name" class="input-md round form-control" required="" name="empresa" placeholder="Empresa *" type="text">
                                        </div>
                                        
                                        <!-- Email -->
                                        <div class="form-group">
                                            <input name="email" id="email" class="input-md round form-control" pattern=".{5,100}" required="" placeholder="E-mail *" type="email">
                                        </div>

                                        <div class="form-group">
                                            <input id="email" class="input-md round form-control" required="" name="telefono" placeholder="Tèlefon *" type="text">
                                        </div>

                                        

                                        <div class="form-group">
                                            <input id="email" class="input-md round form-control" required="" name="web" placeholder="Web" type="text">
                                        </div>

                                        <div class="form-group">
                                            <input id="email" class="input-md round form-control" required="" name="nombre" placeholder="Persona de contacte *" type="text">
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="cf-right-col">
    <div class="form-group">
                                            <input id="email" class="input-md round form-control" required="" name="departamento" placeholder="Departament *" type="text">
                                        </div><div class="form-group">
                                            <input id="email" class="input-md round form-control" required="" name="carrec" placeholder="Càrrec *" type="text">
                                        </div><div class="form-group">
                                            <input id="email" class="input-md round form-control" required="" name="espacio" placeholder="Espai desitja m2 *" type="text">
                                        </div>
                                        
                                        <!-- Message -->
                                        <div class="form-group">                                            
                                            <textarea id="message" class="input-md round form-control" style="height: 84px;" placeholder="BREU DESCRIPCIÓ DE L'ACTIVITAT *" name="mensaje"></textarea>
                                        </div>
<div class="form-group">
                                            <input id="email" class="input-md round" pattern=".{5,100}" required="" name="politicas" placeholder="* Persona de contacte" type="checkbox"> Accepto les <a href="[base_url]p/avis-legal">condicions legals</a>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                                <div class="clearfix">
                                    
                                    <div class="cf-left-col">
                                        
                                        <!-- Inform Tip -->                                        
                                        <div class="form-tip pt-20">
                                            <i class="fa fa-asterisk"></i> Tots els camps són obligatoris</div>
                                        
                                    </div>
                                    
                                    <div class="cf-right-col">
                                        
                                        <!-- Send Button -->
                                        <div class="align-right pt-10">
                                            <input name="tipo" value="contrato" type="hidden">
<button id="submit_btn" class="submit_btn btn btn-mod btn-gray btn-medium btn-round" style="">Enviar</button>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                                
                                
                                <div id="result"></div>
                            </form>

                            <!-- Divider -->
                            <hr class="mt-40 mb-40 ">                            

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text editContent" style="text-align: center; content: none; color: rgb(255, 255, 255); font-size: 16px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, arial, sans-serif; "><p>Omplint aquest formulari accepta que les seves dades siguin incloses a la base de dades propietat de Fira d’Igualada, amb domicili al carrer Piera, núm.24 d’Igualada -08700,   d’acord amb la Llei Orgànica 15/1999, de 13 de desembre, sobre protecció de Dades de Caràcter Personal (LOPD). Les dades que vostè faciliti en aquest formulari seran publicades en un fitxer  al qual tenen accés únicament els expositors d'aquesta fira.</p></div>
                                </div>
                            </div>
                    <!-- End Row -->
                </div>
            </section>


            
            <div>[footer]</div>