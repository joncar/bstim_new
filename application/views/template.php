<!doctype html>
<html lang="en">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': $description ?>" /> 	
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<!-- CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/style.css">
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/style-responsive.css">
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/animate.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/vertical-rhythm.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/magnific-popup.css">
	<link rel="stylesheet" href="<?= base_url() ?>theme/theme/css/YTPlayer.css">
	<script>var URL = '<?= base_url() ?>';</script>
  <link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
</head>

<body class="appear-animate">
	<!-- Page Loader -->        
    <div class="page-loader">
        <div class="loader">Loading...</div>
    </div>
    <!-- End Page Loader -->
    <div class="contenido" style="display: none">
		<?php $this->load->view($view); ?>	
	</div>
	<?php $this->load->view('includes/template/scripts') ?>
</body>
</html>