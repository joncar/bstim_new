/* ---------------------------------------------
 Contact form
 --------------------------------------------- */
/*$(document).ready(function(){
    $("#submit_btn").click(function(){
        
        //get input field values
        var user_name = $('input[name=name]').val();
        var user_email = $('input[name=email]').val();
        var user_message = $('textarea[name=message]').val();
        
        //simple validation at client's end
        //we simply change border color to red if empty field using .css()
        var proceed = true;
        if (user_name == "") {
            $('input[name=name]').css('border-color', '#e41919');
            proceed = false;
        }
        if (user_email == "") {
            $('input[name=email]').css('border-color', '#e41919');
            proceed = false;
        }
        
        if (user_message == "") {
            $('textarea[name=message]').css('border-color', '#e41919');
            proceed = false;
        }
        
        //everything looks good! proceed...
        if (proceed) {
            //data to be sent to server
            var post_data = new FormData(document.getElementById('contact_form'));
            
            //Ajax post data to server
            $.post(URL+'paginas/frontend/contacto', post_data, function(response){                                            
                $("#result").hide().html(response.text).slideDown();
            });
            
        }
        
        return false;
    });
    
    //reset previously set border colors and hide all message on .keyup()
    $("#contact_form input, #contact_form textarea").keyup(function(){
        $("#contact_form input, #contact_form textarea").css('border-color', '');
        $("#result").slideUp();
    });
    
});*/

function contacto(f){
    var post_data = new FormData(f);           
    //Ajax post data to server
    $.ajax({
        url: URL+'paginas/frontend/contacto',
        data: post_data,
        context: document.body,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success:function(response){ 
            response = JSON.parse(response);                                           
            $("#result").hide().html(response.text).slideDown();
        }
    })

    return false;
}
