<!-- Contact Section -->
            <!-- Contact Section -->
            <section class="page-section bg-gray-lighter" id="contact">
                <div class="container relative">
                    
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="section-text align-center mb-70 mb-xs-40"><h2>CONTACTE</h2></div>
                        </div>
                    </div>
                    
                    <div class="row mb-60 mb-xs-40">
                        
                        <div class="col-md-10 col-md-offset-2">
                            <div class="row mb-60 mb-xs-40">
                                
                                <div class="col-md-12">
                                    <div class="row">
                                        
                                        <!-- Phone -->
                                        <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                                            <div class="contact-item">
                                                <div class="ci-icon">
                                                    <i class="fa fa-phone editContent" style="outline: medium none currentcolor; cursor: inherit;" data-selector=".editContent"></i>
                                                </div>
                                                <div class="ci-title font-alt editContent" style="outline: currentcolor none medium; cursor: inherit;" data-selector=".editContent">
                                                    TELÈFON
                                                </div>
                                                <div class="ci-text editContent" style="outline: currentcolor none medium; cursor: inherit;" data-selector=".editContent">
                                                    <a href="tel:+34938040102">+34 93 804 01 02</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Phone -->
                                        
                                        <!-- Address -->
                                        <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                                            <div class="contact-item">
                                                <div class="ci-icon">
                                                    <i class="fa fa-map-marker editContent" style="outline: medium none currentcolor; cursor: inherit;" data-selector=".editContent"></i>
                                                </div>
                                                <div class="ci-title font-alt editContent" style="outline: currentcolor none medium; cursor: inherit;" data-selector=".editContent">
                                                    ADREÇA
                                                </div>
                                                <div class="ci-text editContent" style="outline: currentcolor none medium; cursor: inherit;" data-selector=".editContent">
                                                    Recinte l'escorxador, <br/> c/ Prat de la Riba, 47. Igualada (BCN)
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Address -->
                                        
                                        <!-- Email -->
                                        <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                                            <div class="contact-item">
                                                <div class="ci-icon">
                                                    <i class="fa fa-envelope editContent" style="outline: medium none currentcolor; cursor: inherit;" data-selector=".editContent"></i>
                                                </div>
                                                <div class="ci-title font-alt editContent" style="outline: medium none currentcolor; cursor: inherit;" data-selector=".editContent">
                                                    EMAIL
                                                </div>
                                                <div class="ci-text editContent" style="outline: medium none currentcolor; cursor: inherit;" data-selector=".editContent">
                                                    <a href="mailto:info@bstim.cat">info@bstim.cat</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Email -->
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                    
                    <!-- Contact Form -->
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div id="result"></div>
                            <form class="form" onsubmit="return contacto(this)">
                                <div class="clearfix">
                                    
                                    <div class="cf-left-col">
                                        
                                        <!-- Name -->
                                        <div class="form-group">
                                            <input id="name" class="input-md round form-control" required="" placeholder="Nom *" name="nombre" type="text">
                                        </div>
                                        <div class="form-group">
                                            <input id="telefono" class="input-md round form-control" required="" placeholder="Telèfon *" name="telefono" type="text">
                                        </div>
                                        <!-- Email -->
                                        <div class="form-group">
                                            <input name="email" id="email" class="input-md round form-control" required="" placeholder="E-mail *" type="email">
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="cf-right-col">
                                        
                                        <!-- Message -->
                                        <div class="form-group">
                                            <textarea name="message" id="message" class="input-md round form-control" style="height: 89px;" placeholder="Missatge *"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input name="politicas" type="checkbox"> <span class="form-tip pt-20">ACEPTO <a href="<?= base_url() ?>p/aviso-legal" style="color:#777">POLÍTCA DE PRIVACITAT</a></span>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                                <div class="clearfix">
                                    
                                    <div class="cf-left-col">
                                        
                                        <!-- Inform Tip -->
                                        <div class="form-tip pt-20">
                                        <i class="fa fa-asterisk"></i> Tots els camps són obligatoris</div>
                                        
                                    </div>
                                    
                                    <div class="cf-right-col">
                                        
                                        <!-- Send Button -->
                                        <div class="align-right pt-10">
                                            <input name="tipo" value="contacto" type="hidden">
                                            <button class="submit_btn btn btn-mod btn-medium btn-round" id="submit_btn" style="outline: currentcolor none medium; cursor: inherit;" data-selector="a.btn, button.btn">ENVIAR</button>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </form>
                            
                        </div>
                    </div>
                    <!-- End Contact Form -->
                    
                </div>
                <div class="local-scroll">
                 <a href="#top" class="link-to-top" style=""><i class="fa fa-caret-up"></i></a>
             </div>
            </section>
