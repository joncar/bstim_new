

    <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter -->
            <!-- Home Section -->
            
            <!-- End Home Section -->
            
            
            <!-- About Section -->
            
            
            <!-- Divider -->
            
            <!-- End Divider -->
            
            <!-- Services Section -->
            

            <!-- End Services Section -->
            <!-- Home Section -->
            
            <!-- Google Map -->
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            
            <!-- End About Section -->

            <!-- Section -->
            
            <!-- End Section -->

            
            <!-- End Features Section -->

            <!-- Section -->
            
            <!-- End Section -->
            

            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            
            

            

            <!-- Section -->
            

            

            <!-- Google Map -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            

            

            


            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            


            


            

            


            
                        
            
            
            
             <!-- Section -->
            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
             <!-- Section -->
            
            
            
                      
            <!-- End Head Section -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            <!-- Foter -->
            
            <!-- End Foter -->
        	<!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- Google Map -->
            
            <!-- End Google Map -->
            
            
            <!-- Contact Section -->
            
            <!-- End Contact Section -->
            
            
            
            <!-- Section -->
            
            
            
            
            
           
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            <!-- Section -->
            

			
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            

            <!-- Google Map -->
            


            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            
             <!-- Google Map -->
            


            
            
            <!-- Section -->
            

			
            
            <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter -->
            <!-- Home Section -->
            
            <!-- End Home Section -->
            
            
            <!-- About Section -->
            
            
            <!-- Divider -->
            
            <!-- End Divider -->
            
            <!-- Services Section -->
            

            <!-- End Services Section -->
            <!-- Home Section -->
            
            <!-- Google Map -->
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            
            <!-- End About Section -->

            <!-- Section -->
            
            <!-- End Section -->

            
            <!-- End Features Section -->

            <!-- Section -->
            
            <!-- End Section -->
            

            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            
            

            

            <!-- Section -->
            

            

            <!-- Google Map -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            

            

            


            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            


            


            

            


            
                        
            
            
            
             <!-- Section -->
            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
             <!-- Section -->
            
            
            
                      
            <!-- End Head Section -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            <!-- Foter -->
            
            <!-- End Foter -->
        	<!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- Google Map -->
            
            <!-- End Google Map -->
            
            
            <!-- Contact Section -->
            
            <!-- End Contact Section -->
            
            
            
            <!-- Section -->
            
            
            
            
            
           
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            <div>[menu]</div>
            <!-- Section -->
            <section class="bg bg1 page-section fixed-height-small pt-0 pb-0 bg-dark-alfa-30" data-background="<?= base_url() ?>theme/theme/images/finales/fons.jpg" style="background-image: url(&quot;images/finales/fons.jpg&quot;); ">
                <div class="js-height-parent container relative" style="height: 0px;">
                    
                    <div class="home-content">
                        <div class="home-text">

                            
                            <h2 class="hs-line-14 font-alt mb-50 mb-xs-30 editContent" style="">
                                Aviso legal
                            </h2>
                            
                        </div>
                    </div>
                    
                </div>
            </section>

			<section class="page-section">
                <div class="container relative">
                    <div class="row">
                    	<div class="col-sm-12 mb-40">
                            <div class="text editContent" style="">
                                <h3 class="uppercase">AVÍS LEGAL</h3><p>L’Administració permet la reutilització dels continguts i de les dades per a tot el món i sense cap mena de limitació temporal ni restricció més enllà de les condicions bàsiques establertes en l’article 8 de la Llei 37/2007 (citació de la font, no alteració ni desnaturalització de la informació i especificació de la data d’última actualització), i sempre que no es contradigui amb la llicència o avís que pugui tenir una obra i que és la que preval.</p><p> </p><p> </p><p><b>MODALITATS DE REUTILITZACIÓ DE LA INFORMACIÓ A FIRA D’IGUALADA</b></p><p>La Ley 37/2007, del 16 de novembre, sobre reutilitzación de la información del sector público, regula la reutilización de la información pública de que disponen las administraciones y organismos en que participan mayoritariamente, es decir, el derecho de todos los agentes potenciales del mercado a la reutilización de la información de las instancias públicas. De conformidad con esta normativa, la Administración permite la reproducción, distribución y comunicación pública de la obra y, además, la transformación de la obra para hacer obras derivadas, por todo el mundo y sin ninguna limitación temporal, siempre y cuando no se contradiga con la licencia o aviso que pueda tener una obra y que es la que prevalece.</p><p>Para reutilizar la información, deben seguirse las siguientes condiciones: a) No desnaturalizar el sentido de la información. b) Citar siempre la fuente de la información. c) Mencionar la fecha de la última actualización de la información. Se debe citar siempre al autor o al titular de los derechos: Fira d’Igualada.</p><p> </p><p><b>ALTRES MODALITATS DE REUTILITZACIÓ</b></p><p>Asimismo, la reutilización se puede limitar por la tutela de otros bienes prioritarios, como la protección de los datos personales, la intimidad o los derechos de protección intelectual de terceros. La reutilización de obras protegidas por la propiedad intelectual se puede formalizar mediante el uso de licencias de difusión abierta, como las de Creative Commons, que ceden determinados derechos de explotación de las obras. En los contenidos en los que se aplique este tipo de licencias, se permite la reutilización en las condiciones que se establezcan en las mismas.</p><p>No se autoriza en ningún caso el uso de logotipos, marcas, escudos y símbolos distintivos de la Fira d’Igualada en publicaciones y webs que no estén participados o patrocinados por esta institución. Estos elementos de identidad corporativa son propiedad exclusiva de Fira d’Igualada y están protegidos por la legislación vigente aplicable.</p><p> </p><p> </p><p><b>PRIVACITAT: POLÍTICA DE  PROTECCIÓ DE DADES DE CARÀCTER PERSONAL</b></p><p>Fira d’Igualada  garanteix la confidencialitat de les dades personals que es faciliten a través de les pàgines d’aquest web en el termes establerts per la Llei orgànica 15/1999, de 13 de desembre, de protecció de dades de caràcter personal (LOPD), implementant les mesures de seguretat necessàries i adequades al tipus de dades personals, d’acord amb el Reial decret 1720/2007, de 21 de desembre, pel qual s’aprova el Reglament de desenvolupament de la LOPD. Les dades que es requereixin seran les estrictament necessàries, adequades i pertinents per a la finalitat per a la qual es recullin i seran sotmeses a un tractament automatitzat. A cada pàgina que contingui formularis que recullin dades de caràcter personal, s’informarà, de manera expressa, de l’existència d’un fitxer o tractament de dades de caràcter personal, de la finalitat de la seva recollida i de les persones destinatàries de la informació, de les possibles cessions de dades, de la identitat i adreça del responsable del fitxer, així com de la possibilitat d’exercir els drets d’accés, rectificació, cancel·lació i oposició sobre aquestes dades i la manera d’exercir-los. En cas que en algun formulari que reculli dades de caràcter personal no s’explicitin aquests drets o la manera d’exercir-los, podeu comunicar-ho a: La Direcció General d’Atenció Ciutadana i Difusió, situada a la Via Laietana, 14, 3r, 08003 Barcelona. Aquest òrgan notificarà la incidència a la unitat responsable de la pàgina en qüestió perquè ho esmeni i doni resposta a la vostra petició. Tanmateix, si voleu exercir els drets d’accés, rectificació, cancel·lació i oposició sobre alguna de les vostres dades personals (incloses imatges o gravacions de veu), que poguessin aparèixer en alguna pàgina informativa dels portals de Fira d’Igualada, que no sigui un formulari, us podeu adreçar a Fira d’Igualada, C/ Piera, 24, 08700, Igualada o al mail general@firaigualada.org . D’altra banda, les galetes (cookies) s’utilitzen únicament amb finalitats estadístiques, ja que proporcionen informació rellevant als responsables del portal per millorar el servei a la ciutadania.</p></div>
                        </div>
                     </div>
                </div>
            </section>
            
            <div>[footer]</div>