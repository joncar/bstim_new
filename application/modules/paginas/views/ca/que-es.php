

    <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter -->
            <!-- Home Section -->
            
            <!-- End Home Section -->
            
            
            <!-- About Section -->
            
            
            <!-- Divider -->
            
            <!-- End Divider -->
            
            <!-- Services Section -->
            

            <!-- End Services Section -->
            <!-- Home Section -->
            
            <!-- Google Map -->
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            
            <!-- End About Section -->

            <!-- Section -->
            
            <!-- End Section -->

            
            <!-- End Features Section -->

            <!-- Section -->
            
            <!-- End Section -->
            

            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            
            

            

            <!-- Section -->
            

            

            <!-- Google Map -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            

            

            


            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            


            


            

            


            
                        
            
            
            
             <!-- Section -->
            
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
             <!-- Section -->
            
            
            
                      
            <!-- End Head Section -->
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            <!-- Foter -->
            
            <!-- End Foter -->
        	<!-- Section -->
            
            <!-- End Section -->
            
            
            
            
            <!-- Google Map -->
            
            <!-- End Google Map -->
            
            
            <!-- Contact Section -->
            
            <!-- End Contact Section -->
            
            
            
            <!-- Section -->
            
            
            
            
            
           
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            <!-- Section -->
            

			
            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            

            <!-- Google Map -->
            


            
            
            <!-- Section -->
            
            <!-- End Section -->
            
            
            
            

            
             <!-- Google Map -->
            


            
            
            <!-- Section -->
            

			
            
            <!-- Page Loader -->        
        
        <!-- End Page Loader -->
        
        <!-- Page Wrap -->
        <!-- Foter -->
            
            <!-- End Foter -->
            <!-- Home Section -->
            
            <!-- End Home Section -->
            
            
            <!-- About Section -->
            
            
            <!-- Divider -->
            
            <!-- End Divider -->
            
            <!-- Services Section -->
            

            <!-- End Services Section -->
            <!-- Home Section -->
            
            <!-- Google Map -->
            <div>[menu]</div>
            <!-- Section -->
            <section class="bg bg1 page-section fixed-height-small pt-0 pb-0 bg-dark-alfa-30" data-background="<?= base_url() ?>theme/theme/images/finales/fons.jpg" style="background-image: url(&quot;images/finales/fons.jpg&quot;); ">
                <div class="js-height-parent container relative" style="height: 0px;">
                    
                    <div class="home-content">
                        <div class="home-text">

                            
                            <h2 class="hs-line-14 font-alt mb-50 mb-xs-30 editContent" style="">
                                Qué ès?
                            </h2>
                            
                        </div>
                    </div>
                    
                </div>
            </section>
            <!-- End Section -->
            
            
            
            
            <!-- About Section -->
            <section class="page-section" id="about">
                <div class="container relative">
                    
                    <div class="section-text mb-60 mb-sm-40">
                        <h3 class="uppercase" style="color: black;">QUÈ ÉS BSTIM?</h3>
                        <div class="row">
                            
                            <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30 editContent" style="">
                                <p>BSTIM és un saló professional que pretén donar resposta a les necessitats d’aprovisionament en proximitat de peça acabada de punt de marques i distribuïdors del sector de la moda.</p>
                            </div>
                            <div class="col-md-4 col-sm-6 mb-sm-50 mb-xs-30 editContent" style="">
                                <p>La nostra missió és facilitar l’establiment de relacions comercials entre empreses industrials tèxtils, principalment del punt però també d’altres serveis agregats, i gestors de produccions de productes de moda amb marques i distribuïdors europeus.</p>
                            </div>
                            <div class="col-md-4 col-sm-12 mb-sm-50 mb-xs-30">
                                <div style="font-size: 11px;color: #000;text-align: left;">DISTRIBUÏDORS, MARQUES DE MODA, DISSENYADORS I BOTIGUES</div>
                                <!-- Bar Item -->
                                <div class="progress tpl-progress">
                                    <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50" style="width: 50%;">
                                        <span>50%</span>
                                    </div>
                                </div>
                                <!-- End Bar Item -->
                                <div style="font-size: 11px;color: #000;text-align: left;">EMPRESES INDUSTRIALS I PROFESSIONALS DEL SECTOR TÈXTIL</div>
                                <!-- Bar Item -->
                                <div class="progress tpl-progress">
                                    <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="25" style="width: 25%;">
                                        <span>25%</span>
                                    </div>
                                </div>
                                <!-- End Bar Item -->
                                
                                <!-- Bar Item -->
                                
                                <!-- End Bar Item -->
                                 
                            </div>
                            
                        </div>
                    </div>
                    
                    
                    <!-- Counters -->
                    <div class="count-wrapper">
                        <div class="row">
                            
                            
                            <div class="col-xs-12 col-sm-4">
                                <div class="count-number editContent" style="">70</div>
                                <div class="count-descr font-alt">
                                    <i class="fa fa-bullhorn"></i>
                                    <span class="count-title editContent" style="">Expositors a l'última edició</span>
                                </div>
                            </div>
                            
                            
                            
                            <div class="col-xs-12 col-sm-4">
                                <div class="count-number editContent" style="">4000000</div>
                                <div class="count-descr font-alt">
                                    <i class="fa fa-area-chart editContent" style=""></i>
                                    <span class="count-title editContent" style="">d'euros facturats generats al negoci</span>
                                </div>
                            </div>
                            
                            
                            
                            <div class="col-xs-12 col-sm-4">
                                <div class="count-number editContent" style="">1400</div>
                                <div class="count-descr font-alt">
                                    <i class="fa fa-binoculars editContent" style=""></i>
                                    <span class="count-title editContent" style="">visitants a l'edició 2016</span>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                    <!-- End Counters -->
                    
                </div>
            </section>
            <!-- End About Section -->

            <!-- Section -->
            <section class="bg bg1 page-section fixed-height-small pt-0 pb-0 bg-dark-alfa-30" style="background-image: url(&quot;images/finales/tots-u4515-fr.png&quot;); " data-background="<?= base_url() ?>theme/theme/images/finales/tots-u4515-fr.png">
                <div class="js-height-parent container relative" style="height: 0px;">
                    
                    <div class="home-content">
                        <div class="home-text">

                            
                            <h2 class="hs-line-14 font-alt mb-50 mb-xs-30 editContent" style="">Per què una fira de productors  locals?</h2>
                            
                        </div>
                    </div>
                    
                </div>
            </section>
            <!-- End Section -->

            <section class="page-section">
                <div class="container relative">
                    
                    
                    
                    <!-- Features Grid -->
                    <div class="row multi-columns-row alt-features-grid">
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center" style="margin-top: 0px;">
                                <div class="alt-features-icon">
                                    <img src="<?= base_url() ?>theme/theme/images/finales/icono1.svg" style="width: 80px; ">
                                </div>
                                <h3 class="alt-features-title font-alt editContent medium-editor-element" style="">RAPIDESA</h3>

                                
                            <div style="" class="alt-features-descr align-justify editContent"><p>Sèries més curtes, amb disseny personalitzat i properes a les necessitats i desitjos dels clients.</p><p>La compra en proximitat facilita respondre a les necessitats dels consumidors oferint-los els productes que precisen en cada moment, evitant estocs i costos financers.</p></div></div>
                        </div>
                        <!-- End Features Item -->
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center" style="margin-top: 0;">
                                <div class="alt-features-icon">
                                    <img src="<?= base_url() ?>theme/theme/images/uploads/1/icono_2.svg" style="width: 80px; border-radius: 0px; border-style: none; border-width: 1px; ">
                                </div>
                                <h3 class="alt-features-title font-alt editContent medium-editor-element" style="">PROVEÏDORS DE CONFIANÇA</h3>
                                <div style="" class="alt-features-descr align-justify editContent"><p>Diferents fonts destaquen en els darrers anys la importància de l’anomenat “nearshoring”, és a dir, establir relacions duradores amb</p><p>proveïdors de confiança, propers i experts, per contribuir al desenvolupament dels productes de la marca.</p></div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center" style="margin-top: 0;">
                                <div class="alt-features-icon">
                                    <img src="<?= base_url() ?>theme/theme/images/finales/icono3.svg" style="width: 80px; ">
                                </div>
                                <h3 class="alt-features-title font-alt editContent" style="">INNOVACIÓ</h3>
                                <div style="" class="alt-features-descr align-justify editContent"><p>És impossible innovar en producte sense una indústria de proximitat. Desenvolupar nous productes requereix de capacitat industrial propera (tecnologies i processos) i sobre tot coneixement (Know How).</p></div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        
                    </div>
                    <!-- End Features Grid -->


                    <!-- Features Grid -->
                    <div class="row multi-columns-row alt-features-grid">
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center" style="margin-top: 0;">
                                <div class="alt-features-icon">
                                    <img src="<?= base_url() ?>theme/theme/images/finales/icono4.svg" style="width: 80px; ">
                                </div>
                                <h3 class="alt-features-title font-alt editContent" style="">CAPACITAT</h3>
                                <div style="" class="alt-features-descr align-justify editContent"><p>Estudis del Centre d’Informació Tèxtil (Cityc) i del Centre tecnològic FITEX estimen que la industria tèxtil a Espanya té capacitat per assumir un 30% addicional a l’ocupació actual. Hi ha tecnologies i coneixement al servei de marques i distribuïdors.</p></div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center" style="margin-top: 0;">
                                <div class="alt-features-icon">
                                    <img src="<?= base_url() ?>theme/theme/images/finales/icono5.svg" style="width: 80px; ">
                                </div>
                                <h3 class="alt-features-title font-alt editContent" style="">MODA SOSTENIBLE</h3>
                                <div style="" class="alt-features-descr align-justify editContent"><p>Els expositors de BSTIM disposen d’establiments operatius a territoris de proximitat, principalment a Espanya, i en conseqüència compleixen amb les normes socials i mediambientals regulades per la Unió Europea.</p></div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center" style="margin-top: 0;">
                                <div class="alt-features-icon">
                                    <img src="<?= base_url() ?>theme/theme/images/finales/icono6.svg" style="width: 80px; ">
                                </div>
                                <h3 class="alt-features-title font-alt editContent" style="">internacionalizació</h3>
                                <div style="" class="alt-features-descr align-justify editContent"><p>Els mercats exteriors demanen cada cop més productes amb qualitat, disseny i manufactura europea: “Made in Europe”.</p></div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        
</div><div class="row multi-columns-row alt-features-grid"><div class="col-md-12"><div class="alt-features-item align-center" style="margin-top: 0;">
                                <div class="alt-features-icon">
                                    <img src="<?= base_url() ?>theme/theme/images/uploads/1/fresh.svg" style="width: 120px; ">
                                </div>
                                <h3 class="alt-features-title font-alt editContent" style="content: none; color: rgb(17, 17, 17); font-size: 14px; background-color: rgba(0, 0, 0, 0); font-family: Dosis, arial, sans-serif; ">Fresh &amp; fashion<br></h3>
                                <div class="alt-features-descr align-center editContent" style=""><p>Per a potenciar aquesta rapidesa, la nova edició de BSTIM comptarà, un cop més, amb l’espai Fresh Fashion, pensat especialment per a donar resposta a les marques que necessiten posar col·leccions al mercat en terminis curts.</p></div>
                            </div></div></div>
                    <!-- End Features Grid -->
                        
                </div>
            </section>
            <!-- End Features Section -->

            <!-- Section -->
            <section class="page-section bg-dark">
                <div class="container relative">

                    <div class="row" style="margin-bottom: 50px">
                        <div class="col-md-6" style="text-align: justify;">
                            <img src="<?= base_url() ?>theme/theme/images/nube.svg" style="margin-bottom: 20px; width: 70px;">
                            <h3 style="" class="mt-0 mb-30 mb-xxs-10 editContent">QUI EXPOSA A BSTIM?</h3>
                            <p class="editContent" style="font-weight: initial; ">Empreses industrials que fabriquen peces acabades de confecció, gènere de punt i productes de bany amb capacitat per sevir producte acabat i adaptar-se a diferents volums i condicions de fabricació. També es pretén acollir a empreses dedicades a la gestió de la producció de peces tèxtils que tenen un establiment permanent a l’Estat Espanyol i un equip professional dedicat al desenvolupament i gestió de produccions per marques de moda i distribuïdors.</p>
                        </div>
                        <div class="col-md-6 mb-sm-40" style="text-align: justify;">
                            <img src="<?= base_url() ?>theme/theme/images/icono-add.svg" style="margin-bottom: 13px; width: 53px; ">
                            <h3 style="" class="mt-0 mb-30 mb-xxs-10 editContent">A QUI VA DIRIGIT?</h3>
                            <p class="editContent" style="font-weight: initial; ">BSTIM està ideat pensant en oferir solucions a diferents perfils professionals: RESPONSABLES D’APROVISIONAMENT I COMPRADORS de producte acabat, dissenyadors, responsables de producte i responsables de col•lecció o directors artístics de marques de moda i distribuïdors del sector tèxtil i moda d’arreu d’Europa.</p>
                            <p class="editContent" style="font-weight: initial; "><b>DISSENYADORS INDEPENDENTS que</b> desenvolupen la seva pròpia col•lecció.</p>
                            <p class="editContent" style="font-weight: initial;"><b>CADENES DE BOTIGUES</b> interessades en identificar productors per cobrir necessitats de producte en els seus establiments comercials.</p>
                            <p class="editContent" style="font-weight: initial; "><b>ALUMNES D’ESCOLES DE DISSENY i</b> Escoles de negoci, màrqueting i comunicació dedicades al sector de la moda. <a href="[base_url]p/expositors">Inscriu-te ja!</a></p>
                        </div>
                    </div>
                    
                    <div class="row">
                                                                
                        <div class="col-md-6" style="text-align: justify;">
                            
                            <!-- About -->
                            <div class="text">
                                <img src="<?= base_url() ?>theme/theme/images/icono-herramientas.svg" style="margin-bottom: 20px; width: 70px;">
                                <h3 style="color: white; " class="mt-0 mb-30 mb-xxs-10 editContent">GRUP D’ADVISORS DE BSTIM</h3>
                                <p class="editContent" style="color: rgb(255, 255, 255); font-size: 14px; background-color: rgba(0, 0, 0, 0); font-family: &quot;Open Sans&quot;, arial, sans-serif; margin: 0px; font-weight: initial; ">La fira BSTIM compta amb un Grup d’Advisors format per experts del sector de la moda amb l’objectiu que puguin aportar a la fira la seva experiència i els seus coneixements del sector per tal que sigui el màxim  de productiva per a visitants i expositors. Tots ells van acceptar col•laborar amb BSTIM de manera totalment desinteressada. Van formar part del grup d'advisors de l'edició 2015, els següents experts del sector:<br><br></p><p style="color: #fff;font-size: 14px;"><b>Francesc Maristany,</b> president de MODACC<b><br>Miquel Rodríguez,</b> gerent de CCAM de la Generalitat de Catalunya<b><br>Carmen Torres,</b> Secretària General de la Federación Española de Industrias de la Confección (FEDECON)<b><br>Sílvia Riera,</b> periodista de la revista Modaes<b><br>Aleix Aguilera</b>, fundador del magazine CooltureMag<b><br>Iago Esteve,</b> responsable de Sita Murt<b><br>Silvia Calvo, </b>presidenta de l’Asociación Moda Sostenible<b><br>Pepa Bueno</b>, directora de l’Asociación de Creadores de Moda de España (ACME)<b><br>Igone Bartumeu, </b>comunicació de la marca Desigual<b><br>Javier Goyaneche,</b> fundador d’Ecoalf<b><br>Santi Casanellas,</b> fundador de la marca Referendum</p><p></p>
                                
                            </div>
                            <!-- End About -->
                            
                        </div>


                        <div class="col-md-6 mb-sm-40" style="background: transparent;">
                            
                            <!-- Image -->
                            <div class="work-full-media mt-0 white-shadow wow fadeInUp" style="visibility: hidden; background: transparent none repeat scroll 0% 0%; animation-name: none;">
                                <img src="<?= base_url() ?>theme/theme/images/uploads/1/foto.jpg" alt="" style="">
                            </div>
                            <!-- End Image -->
                            
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Section -->
            <div>[footer]</div>